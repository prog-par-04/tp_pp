#!/bin/bash
make clean; make

for I in {16..64..2}
do
(( ITER=10))
exe/test.exe $I $I $ITER
done

for I in {64..128..4}
do
(( ITER=10))
exe/test.exe $I $I $ITER
done

for I in {128..4096..8}
do
(( ITER=10))
exe/test.exe $I $I $ITER
done


