import matplotlib.pyplot as plt

n_threads = 0
X = list()
Y = list()


if __name__ == "__main__":
    with open("./resultats/out2.txt") as file:
        for line in file.readlines()[:-9]:
            data = line.strip().split(' ')
            if data[0] == "THREADS":
                if n_threads != 0:
                    plt.plot(X, Y, label=("Threads: " + str(n_threads)))
                X = list()
                Y = list()
                n_threads = int(data[1])
                continue
            X.append(int(data[0]))
            Y.append(float(data[1]))
        plt.plot(X, Y, label=("Threads: " + str(n_threads)))
    # plt.plot([50, 50], [min(Y), max(Y)])
    # plt.plot([350, 350], [min(Y), max(Y)])
    # plt.plot([1600, 1600], [min(Y), max(Y)])
    plt.title("Ratio du temps d'execution en fonction de la taille des données")
    plt.xlabel("Taille des données")
    plt.ylabel("Ratio")
    plt.legend()
    plt.show()
