#include <chrono>
#include <random>
#include <iostream>
#include <float.h>
#include <immintrin.h>
#include <omp.h>

double sequentiel(double *A, double *B, double *S, unsigned long int size)
{
    double s = 0;
    for (unsigned long int i = 0; i < size; i++)
    {
        s += A[i] * B[i];
    }
    *S = s;
    return *S;
}

double parallele(double *A, double *B, double *S, unsigned long int size, int nThreads)
{
    omp_set_num_threads(nThreads);
    double s = 0;
#pragma omp parallel
    {
#pragma omp for reduction(+ \
                          : s)
        for (unsigned long int i = 0; i < size; i++)
        {
            s += A[i] * B[i];
        }
    }
    *S = s;
    return *S;
}

int main(int argc, char *argv[])
{

    int nThreads = 2;
    if (argc >= 2)
    {
        nThreads = atoi(argv[1]);
    }

    /* initialize random seed: */
    srand(time(NULL));
    double value1;
    double value2;

    for (unsigned long int size = 1024; size < (2048 * 2048 * 4); size *= 1.2)
    {
        unsigned long int iter = 256 * 1024 * 1024 / size;

        // Création des données de travail
        double *A, *B, *C, S1, S2;
        A = (double *)malloc(size * sizeof(double));
        B = (double *)malloc(size * sizeof(double));
        S1 = 0;
        S2 = 0;

        for (unsigned long int i = 0; i < size; i++)
        {
            A[i] = (double)(rand() % 360 - 180.0);
            B[i] = (double)(rand() % 360 - 180.0);
        }

        std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
        double min_duration = DBL_MAX;
        t0 = std::chrono::high_resolution_clock::now();
        for (auto it = 0; it < iter; it++)
        {
            value1 = sequentiel(A, B, &S1, size);
        }
        t1 = std::chrono::high_resolution_clock::now();
        double seq_duration = std::chrono::duration<double>(t1 - t0).count();
        seq_duration /= (size * iter);

        t0 = std::chrono::high_resolution_clock::now();
        for (auto it = 0; it < iter; it++)
        {
            value2 = parallele(A, B, &S2, size, nThreads);
        }
        t1 = std::chrono::high_resolution_clock::now();
        double par_duration = std::chrono::duration<double>(t1 - t0).count();
        par_duration /= (size * iter);

        std::cout << size << " " << seq_duration / par_duration << std::endl;
        // std::cout << size << " " << seq_duration << " " << par_duration << std::endl;

        /*** Validation ***/
        bool valide = true;
        for (unsigned long int i = 0; i < size; i++)
        {
            if (S1 != S2)
            {
                valide = false;
                std::cout << "Invalid" << S1 << S2 << std::endl;
                break;
            }
        }

        // Libération de la mémoire : indispensable

        free(A);
        free(B);
    }
    return 0;
}
