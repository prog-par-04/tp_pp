import matplotlib.pyplot as plt

X = list()
Y1 = list()
Y2 = list()

def read_file(filename):
  with open(filename) as file:
    for line in file.readlines():
      data = line.strip().split(' ')
      X.append(int(data[0]))
      Y1.append(float(data[1]) * 10**9)
      Y2.append(float(data[2]) * 10**9)

if __name__=="__main__":
  read_file('./Resultats/vec_out.txt')
  plt.plot(X, Y1, label="sequentiel")
  plt.plot(X, Y2, label="parrallele")
  plt.title("Temps d'execution de la recherche min/max en fonction de la taille des vecteurs et de l'algorithme (optimiseur avec vectorisation)")
  plt.xlabel("Taille des vecteurs")
  plt.ylabel("Temps (ns)")
  plt.legend()
  plt.show()

  X = list()
  Y1 = list()
  Y2 = list()
  read_file('./Resultats/novec_out.txt')
  plt.plot(X, Y1, label="sequentiel")
  plt.plot(X, Y2, label="parrallele")
  plt.title("Temps d'execution de la recherche min/max en fonction de la taille des vecteurs et de l'algorithme (optimiseur sans vectorisation)")
  plt.xlabel("Taille des vecteurs")
  plt.ylabel("Temps (ns)")
  plt.legend()
  plt.show()
