#include <chrono>
#include <random>
#include <iostream>
#include <float.h>
#include <values.h>
#include <immintrin.h>

float sequentiel(float *A, float *S, unsigned long int size)
{
    float min = MAXFLOAT;
    float max = MINFLOAT;
    for (unsigned long int i = 0; i < size; i++) {
        if (A[i] > max) {
            max = A[i];
        }
        if (A[i] < min) {
            min = A[i];
        }
    }
    S[0] = max;
    S[1] = min;
    return S[0];
}

float parallele(float *A, float *S, unsigned long int size)
{
    __m256 vec_min = _mm256_set1_ps(MAXFLOAT);
    __m256 vec_max = _mm256_set1_ps(MINFLOAT);
    for (unsigned long i = 0; i < size; i+=8) {
        __m256 a = _mm256_loadu_ps(&A[i]);
        vec_max = _mm256_max_ps(vec_max, a);
        vec_min = _mm256_min_ps(vec_min, a);
    }

    float min = MAXFLOAT;
    float max = MINFLOAT;
    float tmin[8];
    float tmax[8];
    _mm256_storeu_ps(tmin, vec_min);
    _mm256_storeu_ps(tmax, vec_max);
    for (auto i = 0; i < 8; i++) {
        if (tmin[i] < min) {
            min = tmin[i];
        }
        if (tmax[i] > max) {
            max = tmax[i];
        }
    }

    S[0] = max;
    S[1] = min;
    return S[0];
}

int main(int argc, char *argv[])
{
    unsigned long int iter = atoi(argv[1]);

    /* initialize random seed: */

    srand(time(NULL));

    const unsigned long int size = atoi(argv[2]) * atoi(argv[2]);

    // std::cout << iter << " " << size << std::endl;

    // Création des données de travail
    float *A, *S1, *S2;
    A = (float *)malloc(size * sizeof(float));
    S1 = (float *)malloc(2 * sizeof(float));
    S2 = (float *)malloc(2 * sizeof(float));

    for (unsigned long int i = 0; i < size; i++)
    {
        A[i] = (float)(rand() % 360 - 180.0);
    }

    /*** Validation ***/
    sequentiel(A, S1, size);
    parallele(A, S2, size);
    bool valide = false;
    for (unsigned long int i = 0; i < 2; i++)
    {
        if (S1[i] == S2[i])
        {
            valide = true;
        }
        else
        {
            valide = false;
            break;
        }
    }
    // std::cout << "Le résultat est " << std::boolalpha << valide << std::endl;

    std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    double min_duration = DBL_MAX;
    double result = 0;
    for (auto it = 0; it < iter; it++)
    {
        t0 = std::chrono::high_resolution_clock::now();
        result += sequentiel(A, S1, size);
        t1 = std::chrono::high_resolution_clock::now();
        double duration = std::chrono::duration<double>(t1 - t0).count();
        if (duration < min_duration)
            min_duration = duration;
    }

    auto seq_duration = (min_duration / size);

    min_duration = DBL_MAX;
    for (auto it = 0; it < iter; it++)
    {
        t0 = std::chrono::high_resolution_clock::now();
        result += parallele(A, S2, size);
        t1 = std::chrono::high_resolution_clock::now();
        double duration = std::chrono::duration<double>(t1 - t0).count();
        if (duration < min_duration)
            min_duration = duration;
    }

    std::cout << size << " " << seq_duration << " " << (min_duration / size) << " " << result / size << std::endl;

    // Libération de la mémoire : indispensable
    free(A);
    free(S1);
    free(S2);

    return 0;
}
