#include <stdio.h>
#include <iostream>
#include "cuda_runtime.h"
#include "cuda.h"
#include "omp.h"
#include "device_launch_parameters.h"
#include <chrono>

using namespace std;

__global__ void gpu(int n, float *x, float *y, float *s)
{
  int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < n) atomicAdd(s, x[i] * y[i]);
}

void cpu(int n, float *x, float *y, float *s)
{
  #pragma omp parallel for num_threads(12) 
  for (int i=0; i<n; i++)
  {
      *s += x[i] * y[i];
  }
}

void cpu_mono(int n, float *x, float *y, float *s)
{
  for (int i=0; i<n; i++)
  {
      *s = x[i] * y[i];
  }
}


int main(int argc, char *argv[])
{
  int size = 4096;
  if (argc >= 2)
  {
    size = atoi(argv[1]);
  }

  unsigned long int N = size*size*16;
  float *x, *y, *d_x, *d_y, *d_s;
  
  x = (float*)malloc(N*sizeof(float));
  y = (float*)malloc(N*sizeof(float));
  float s_cpu = 0;
  float s_gpu = 0;

  cudaMalloc(&d_x, N*sizeof(float)); 
  cudaMalloc(&d_y, N*sizeof(float));
  cudaMalloc(&d_s, sizeof(float));

  for (int i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
  cpu(N, x, y, &s_cpu);
  std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
  auto cpu_duration = std::chrono::duration<double>(t1-t0).count();

  std::cout << N << " " << cpu_duration << " ";

  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);

  int k = 32; 
  t0 = std::chrono::high_resolution_clock::now();
  gpu<<<(N+k)/k, k>>>(N, d_x, d_y, d_s);  
  t1 = std::chrono::high_resolution_clock::now();
  auto gpu_duration = std::chrono::duration<double>(t1-t0).count();
  cudaMemcpy(&s_gpu, d_s, sizeof(float), cudaMemcpyDeviceToHost);

    float maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = std::max(maxError, s_cpu-s_gpu);
  if (maxError > 0)
  {
    printf("Max error: %f\n", maxError);
  }

  std::cout << gpu_duration << " "; 

  k = 512; 
  t0 = std::chrono::high_resolution_clock::now();
  gpu<<<(N+k)/k, k>>>(N, d_x, d_y, d_s);  
  t1 = std::chrono::high_resolution_clock::now();
  gpu_duration = std::chrono::duration<double>(t1-t0).count();
  cudaMemcpy(&s_gpu, d_s, sizeof(float), cudaMemcpyDeviceToHost);

  maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = std::max(maxError, s_cpu-s_gpu);
  if (maxError > 0)
  {
    printf("Max error: %f\n", maxError);
  }

  std::cout << gpu_duration << " "; 


  k = 1024; 
  t0 = std::chrono::high_resolution_clock::now();
  gpu<<<(N+k)/k, k>>>(N, d_x, d_y, d_s);  
  t1 = std::chrono::high_resolution_clock::now();
  gpu_duration = std::chrono::duration<double>(t1-t0).count();
  cudaMemcpy(&s_gpu, d_s, sizeof(float), cudaMemcpyDeviceToHost);
  
  maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = std::max(maxError, s_cpu-s_gpu);
  if (maxError > 0)
  {
    printf("Max error: %f\n", maxError);
  }

  std::cout << gpu_duration << std::endl;
  
  cudaFree(d_x);
  cudaFree(d_y);
  cudaFree(d_s);
  free(x);
  free(y);
}

