import matplotlib.pyplot as plt
import argparse

X = list()
Y = list()
Y2 = list()
Y3 = list()
Y4 = list()


def read_file(filename):
    with open(filename) as file:
        for line in file.readlines()[4:]:
            data = line.strip().split(' ')
            X.append(int(data[0]))
            Y.append(float(data[1]))
            Y2.append(float(data[1]) / float(data[2]))
            Y3.append(float(data[1]) / float(data[3]))
            Y4.append(float(data[1]) / float(data[4]))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "file_path", help="path to the csv file", default="./output.csv")
    args = parser.parse_args()
    read_file(args.file_path)
    # plt.plot(X, Y, label="CPU")
    plt.plot(X, Y2, linestyle='--', marker='o',
             label="GPU 32 threads par block")
    plt.plot(X, Y3, linestyle='--', marker='o',
             label="GPU 512 threads par block")
    plt.plot(X, Y4, linestyle='--', marker='o',
             label="GPU 1024 threads par block")
    plt.title("Gain GPU / CPU en fonction de la taille des données")
    plt.xlabel("Taille des données")
    plt.ylabel("Gain (temps d'exécution GPU /CPU")
    plt.legend()
    plt.show()
