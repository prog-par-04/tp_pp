mkdir exe
mkdir obj
mkdir include

module load cuda/10.2 

nvcc -std=c++11 -Xcompiler -fopenmp -O3 -o main.exe main.cu
