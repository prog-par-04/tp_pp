#include <chrono>
#include <random>
#include <iostream>
#include <float.h>
#include <immintrin.h>

float sequentiel(float *A, float *B, float *S, unsigned long int size)
{
    for (unsigned long int i = 0; i < size; i++)
    {
        *S += A[i] * B[i];
    }
    return *S;
}

float parallele(float *A, float *B, float *S, unsigned long int size)
{
    __m256 s = _mm256_set1_ps(0);
    for (unsigned long i = 0; i < size; i += 8)
    {
        __m256 a = _mm256_loadu_ps(&A[i]);
        __m256 b = _mm256_loadu_ps(&B[i]);
        s = _mm256_add_ps(s, _mm256_mul_ps(a, b));
    }
    float t[8];
    _mm256_storeu_ps((float *)t, s);
    for (int i = 0; i < 8; ++i)
    {
        *S += t[i];
    }
    return *S;
}

int main(int argc, char *argv[])
{
    unsigned long int iter = atoi(argv[1]);

    /* initialize random seed: */

    srand(time(NULL));

    const unsigned long int size = atoi(argv[2]) * atoi(argv[2]);

    // std::cout << iter << " " << size << std::endl;

    // Création des données de travail
    float *A, *B;
    A = (float *)malloc(size * sizeof(float));
    B = (float *)malloc(size * sizeof(float));
    float S1 = 0;
    float S2 = 0;

    for (unsigned long int i = 0; i < size; i++)
    {
        A[i] = (float)(rand() % 360 - 180.0);
        B[i] = (float)(rand() % 360 - 180.0);
    }

    /*** Validation ***/
    sequentiel(A, B, &S1, size);
    parallele(A, B, &S2, size);
    bool valide = false;
    if (S1 == S2)
    {
        valide = true;
    }
    // std::cout << "Le résultat est " << std::boolalpha << valide << std::endl;

    std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    double min_duration = DBL_MAX;
    for (auto it = 0; it < iter; it++)
    {
        t0 = std::chrono::high_resolution_clock::now();
        sequentiel(A, B, &S1, size);
        t1 = std::chrono::high_resolution_clock::now();
        double duration = std::chrono::duration<double>(t1 - t0).count();
        if (duration < min_duration)
            min_duration = duration;
    }

    auto seq_duration = (min_duration / size);

    min_duration = DBL_MAX;
    for (auto it = 0; it < iter; it++)
    {
        t0 = std::chrono::high_resolution_clock::now();
        parallele(A, B, &S2, size);
        t1 = std::chrono::high_resolution_clock::now();
        double duration = std::chrono::duration<double>(t1 - t0).count();
        if (duration < min_duration)
            min_duration = duration;
    }

    std::cout << size << " " << seq_duration << " " << (min_duration / size) << " " << S2 << std::endl;

    // Libération de la mémoire : indispensable
    free(A);
    free(B);

    return 0;
}
