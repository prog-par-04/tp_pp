#include <iostream>
#include <chrono>


#include "structural_elements.hpp"
#include "container.hpp"
#include "global_parameters.hpp"
#include "ios.hpp"
#include "omp.h"

#define DEBUG 1

void sobel_seq(u_char **Source, u_char **Resultat, unsigned int height, unsigned int width, unsigned int nthreads) {

    for (auto i = 1; i < height-1; i++) {
        for (auto j = 1; j < width-1; j++) {
            if ((i==0)||(i==height-1)||(j==0)||(j==width-1)) {Resultat[i][j]=0;}
            else {
                Resultat[i][j]  = std::abs(Source[i-1][j-1] + Source[i-1][j] + Source[i-1][j+1] - (Source[i+1][j-1] + Source[i+1][j] + Source[i+1][j+1]));
                Resultat[i][j] += std::abs(Source[i-1][j-1] + Source[i][j-1] + Source[i+1][j-1] - (Source[i-1][j+1] + Source[i][j+1] + Source[i+1][j+1]));
            }
        }
    }
}

void sobel_parallele(u_char **Source, u_char **Resultat, unsigned int height, unsigned int width, unsigned int nthreads) {
    omp_set_num_threads(nthreads);
    int steps = (height-1) / nthreads;
    #pragma omp parallel for
    for (auto k = 0; k < nthreads; k ++) {
        int iMax = k == nthreads - 1 ? height - 1 : (k+1) * steps;
        for (auto i = k * steps; i < iMax; i++) {
            for (auto j = 1; j < width - 1; j++) {
                if ((i == 0) || (i == height - 1) || (j == 0) || (j == width - 1)) { Resultat[i][j] = 0; }
                else {
                    Resultat[i][j] = std::abs(Source[i - 1][j - 1] + Source[i - 1][j] + Source[i - 1][j + 1] -
                                              (Source[i + 1][j - 1] + Source[i + 1][j] + Source[i + 1][j + 1]));
                    Resultat[i][j] += std::abs(Source[i - 1][j - 1] + Source[i][j - 1] + Source[i + 1][j - 1] -
                                               (Source[i - 1][j + 1] + Source[i][j + 1] + Source[i + 1][j + 1]));
                }
            }
        }
    }
}

void sobel_parallele_2(u_char **Source, u_char **Resultat, unsigned int height, unsigned int width, unsigned int nthreads) {
    omp_set_num_threads(nthreads);
#pragma omp parallel for collapse(2)
    for (auto i = 1; i < height-1; i++) {
        for (auto j = 1; j < width-1; j++) {
            if ((i==0)||(i==height-1)||(j==0)||(j==width-1)) {Resultat[i][j]=0;}
            else {
                Resultat[i][j]  = std::abs(Source[i-1][j-1] + Source[i-1][j] + Source[i-1][j+1] - (Source[i+1][j-1] + Source[i+1][j] + Source[i+1][j+1]));
                Resultat[i][j] += std::abs(Source[i-1][j-1] + Source[i][j-1] + Source[i+1][j-1] - (Source[i-1][j+1] + Source[i][j+1] + Source[i+1][j+1]));
            }
        }
    }
}

void sobel_parallele_3(u_char **Source, u_char **Resultat, unsigned int height, unsigned int width, unsigned int nthreads) {
    omp_set_num_threads(nthreads);
    for (auto i = 1; i < height-1; i++) {
        #pragma omp parallel for
        for (auto j = 1; j < width-1; j++) {
            if ((i==0)||(i==height-1)||(j==0)||(j==width-1)) {Resultat[i][j]=0;}
            else {
                Resultat[i][j]  = std::abs(Source[i-1][j-1] + Source[i-1][j] + Source[i-1][j+1] - (Source[i+1][j-1] + Source[i+1][j] + Source[i+1][j+1]));
                Resultat[i][j] += std::abs(Source[i-1][j-1] + Source[i][j-1] + Source[i+1][j-1] - (Source[i-1][j+1] + Source[i][j+1] + Source[i+1][j+1]));
            }
        }
    }
}

int main(int argc, char* argv[]) {
    unsigned int height, width;
    unsigned int nthreads = omp_get_max_threads();

    std::string image_filename(argv[1]);
    int ITER =  atoi(argv[2]);
    int nThreads =  atoi(argv[3]);

    get_source_params(image_filename, &height, &width);
    u_char **Source, **Resultat;

	image<u_char> imgSource(height, width, &Source);
    image<u_char> imgResultat(height, width, &Resultat);
       
    auto fail = init_source_image(Source, image_filename, height, width);
    if (fail) {
        std::cout << "Chargement impossible de l'image" << std::endl;
        return 0;
    }

    // Sequentiel
    std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
    for (auto it =0; it < ITER; it++) {
        sobel_seq(Source, Resultat, height, width, nthreads);
    }
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration<double>(t1-t0).count()/ITER;

    // Parallele 1
    t0 = std::chrono::high_resolution_clock::now();
    for (auto it =0; it < ITER; it++) {
        sobel_parallele(Source, Resultat, height, width, nThreads);
    }
    t1 = std::chrono::high_resolution_clock::now();
    auto duration2 = std::chrono::duration<double>(t1-t0).count()/ITER;

    // Parallele 2
    t0 = std::chrono::high_resolution_clock::now();
    for (auto it =0; it < ITER; it++) {
        sobel_parallele_2(Source, Resultat, height, width, nThreads);
    }
    t1 = std::chrono::high_resolution_clock::now();
    auto duration3 = std::chrono::duration<double>(t1-t0).count()/ITER;

    // Parallele 3
    t0 = std::chrono::high_resolution_clock::now();
    for (auto it =0; it < ITER; it++) {
        sobel_parallele_3(Source, Resultat, height, width, nThreads);
    }
    t1 = std::chrono::high_resolution_clock::now();
    auto duration4 = std::chrono::duration<double>(t1-t0).count()/ITER;

    std::cout << nThreads  << " " << duration / duration2 << " " << duration / duration3 << " " << duration / duration4 << std::endl;

    #ifdef DEBUG
        image_filename=std::string("Sobel.pgm");
        save_gray_level_image(&imgResultat, image_filename, height, width);
    #endif
    
    return 0;
}
