import matplotlib.pyplot as plt

X = list()
Y = list()
Y1 = list()
Y2 = list()


def read_file(filename):
    with open(filename) as file:
        for line in file.readlines()[:-9]:
            data = line.strip().split(' ')
            X.append(int(data[0]))
            Y.append(float(data[1]))
            Y1.append(float(data[2]))
            Y2.append(float(data[3]))


if __name__ == "__main__":
    read_file("./resultats/out3.txt")
    plt.plot(X, Y, label="Ligne")
    plt.plot(X, Y1, label="Pixel")
    plt.plot(X, Y2, label="Colonne")
    plt.title("Ratio du temps d'execution en fonction du nombre de threads")
    plt.xlabel("Nombre de threads")
    plt.ylabel("Ratio")
    plt.legend()
    plt.show()
