#PBS -S /bin/bash
#PBS -N tp3_ex2_moyenne
#PBS -e errorJob.txt
#PBS -j oe
#PBS -l walltime=0:10:00
#PBS -l select=1:ncpus=20:cpugen=skylake
#PBS -l place=excl
#PBS -m abe -M david.robert--ansart@student.ecp.fr
#PBS -P progpar


# Load the same modules as for compilation
module load gcc/7.3.0
# module load intel-compilers/2019.3
# Go to the current directory
cd $PBS_O_WORKDIR

for I in {1..32..1}
do
  exe/tp2_ex4.exe ./images/Drone_huge.pgm 100 $I
done

/gpfs/opt/bin/fusion-whereami
date
time sleep 2
