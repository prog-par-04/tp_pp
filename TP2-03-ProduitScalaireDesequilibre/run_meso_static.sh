#PBS -S /bin/bash
#PBS -N tp2_ex3_prodscal_desequilibre_static
#PBS -e errorJob.txt
#PBS -j oe
#PBS -l walltime=0:18:00
#PBS -l select=1:ncpus=20:cpugen=skylake
#PBS -l place=excl
#PBS -m abe -M quentin.verlhac@student.ecp.fr
#PBS -P progpar


# Load the same modules as for compilation
module load gcc/7.3.0
# module load intel-compilers/2019.3
# Go to the current directory
cd $PBS_O_WORKDIR

for I in {4..32..8}
do
  echo "THREADS $I"
  exe/tp2_ex3_static.exe $I
done

/gpfs/opt/bin/fusion-whereami
date
time sleep 2
