import matplotlib.pyplot as plt

n_threads = 0
X = list()
Y = list()


if __name__ == "__main__":
    with open("results/tp2_ex3_prodscal_desequilibre_dynamic_1_per_thread.txt") as file:
        for line in file.readlines():
            data = line.strip().split(' ')
            if data[0] == "THREADS":
                if n_threads != 0:
                    plt.plot(X, Y, label=("Threads: " + str(n_threads)))
                X = list()
                Y = list()
                n_threads = int(data[1])
                continue
            X.append(int(data[0]))
            Y.append(float(data[1]))
    # plt.plot([50, 50], [min(Y), max(Y)])
    # plt.plot([350, 350], [min(Y), max(Y)])
    # plt.plot([1600, 1600], [min(Y), max(Y)])
    plt.title(
        "Accélération du produit scalaire déséquilibré pour un ordonnancement dynamique avec 1 chunk par thread.")
    plt.xlabel("Taille des données")
    plt.ylabel("Ratio")
    plt.legend()
    plt.show()
