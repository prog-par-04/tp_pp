import matplotlib.pyplot as plt
import argparse

X = list()
Y = list()
Y2 = list()

def read_file(filename):
  with open(filename) as file:
    for line in file.readlines()[4:]:
      data = line.strip().split(' ')
      X.append(int(data[0]))
      Y.append(float(data[1]) * 10**9)
      if (len(data) >= 3):
        Y2.append(float(data[2]) * 10**9)

if __name__=="__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("file_path", help="path to the csv file", default="./output.csv")
  parser.add_argument("--cache_exits", dest='cache_exits', help="print cache exit (syntax: --cache_exits True)", type=bool, default=False)
  parser.add_argument("--draw_parallel_line", dest='draw_parallel_line', help="draw parallel line", type=bool, default=False)
  args = parser.parse_args()
  read_file(args.file_path)
  plt.plot(X, Y, label="sequentiel")
  if args.draw_parallel_line:
    plt.plot(X, Y2, label="parrallele")
  if args.cache_exits:
    plt.plot([50, 50], [min(Y), max(Y)])
    plt.plot([350, 350], [min(Y), max(Y)])
    plt.plot([1600, 1600], [min(Y), max(Y)])
  plt.title("Temps d'execution par point en fonction de la taille des données")
  plt.xlabel("Taille des données")
  plt.ylabel("Temps (ns)")
  plt.show()
