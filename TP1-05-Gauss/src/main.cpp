#include <chrono>
#include <random>
#include <iostream>
#include <float.h>
#include <immintrin.h>

float sequentiel(float *A, float *S, unsigned long int size)
{
    for (unsigned long int i = 0; i < size; i++) {
        float a0 = (i == 0) ? 0 : A[i-1];
        float a2 = (i == size-1) ? 0 : A[i+1];
        S[i] = (a0 + 2 * A[i] + a2) / 4;
    }
    return S[0];
}

float parallele(float *A, float *S, unsigned long int size)
{
    __m256 vec2 = _mm256_set1_ps(2);
    __m256 vec4 = _mm256_set1_ps(4);
    for (unsigned long i = 0; i < size; i+=8) {
        __m256 a0;
        __m256 a2;
        if (i == 0) {
            float vec[8] = { 0, A[0], A[1], A[2], A[3], A[4], A[5], A[6]};
            a0 = _mm256_loadu_ps(vec);
        } else {
            a0 = _mm256_loadu_ps(&A[i-1]);
        }

        if (i == size - 1) {
            float vec[8] = { A[i-6], A[i-5], A[i-4], A[i-3], A[i-2], A[i-1], A[i], 0};
            a2 = _mm256_loadu_ps(vec);
        } else {
            a2 = _mm256_loadu_ps(&A[i+1]);
        }
        __m256 a1 = _mm256_loadu_ps(&A[i]);
        __m256 s = _mm256_add_ps(a0, _mm256_mul_ps(a1, vec2));
        s = _mm256_add_ps(s, a2);
        s = _mm256_div_ps(s, vec4);
        _mm256_storeu_ps(&S[i],s);
    }
    return S[0];
}

int main(int argc, char *argv[])
{
    unsigned long int iter = atoi(argv[1]);

    /* initialize random seed: */

    srand(time(NULL));

    const unsigned long int size = atoi(argv[2]) * atoi(argv[2]);

    // std::cout << iter << " " << size << std::endl;

    // Création des données de travail
    float *A, *S1, *S2;
    A = (float *)malloc(size * sizeof(float));
    S1 = (float *)malloc(size * sizeof(float));
    S2 = (float *)malloc(size * sizeof(float));

    for (unsigned long int i = 0; i < size; i++)
    {
        A[i] = (float)(rand() % 360 - 180.0);
    }

    /*** Validation ***/
    sequentiel(A, S1, size);
    parallele(A, S2, size);
    bool valide = false;
    for (unsigned long int i = 0; i < size; i++)
    {
        if (S1[i] == S2[i])
        {
            valide = true;
        }
        else
        {
            valide = false;
            break;
        }
    }
    // std::cout << "Le résultat est " << std::boolalpha << valide << std::endl;

    std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    double min_duration = DBL_MAX;
    double result = 0;
    for (auto it = 0; it < iter; it++)
    {
        t0 = std::chrono::high_resolution_clock::now();
        result += sequentiel(A, S1, size);
        t1 = std::chrono::high_resolution_clock::now();
        double duration = std::chrono::duration<double>(t1 - t0).count();
        if (duration < min_duration)
            min_duration = duration;
    }

    auto seq_duration = (min_duration / size);

    min_duration = DBL_MAX;
    for (auto it = 0; it < iter; it++)
    {
        t0 = std::chrono::high_resolution_clock::now();
        result += parallele(A, S2, size);
        t1 = std::chrono::high_resolution_clock::now();
        double duration = std::chrono::duration<double>(t1 - t0).count();
        if (duration < min_duration)
            min_duration = duration;
    }

    std::cout << size << " " << seq_duration << " " << (min_duration / size) << " " << result / size << std::endl;

    // Libération de la mémoire : indispensable
    free(A);
    free(S1);
    free(S2);

    return 0;
}
