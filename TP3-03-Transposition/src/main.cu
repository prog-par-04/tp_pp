#include <stdio.h>
#include <iostream>
#include <chrono>

#include "cuda_runtime.h"
#include "cuda.h"
#include "omp.h"

#include "gpu_transpose.cuh"

#include "container.cuh"
#include "ios.cuh"

#include "parameters.cuh"


#define DEBUG 1

void cpu_transpose(u_char **Source, u_char **Resultat, unsigned int height, unsigned int width, unsigned int nthreads) {

    for (auto i = 0; i < width; i++) {
        for (auto j = 0; j < height; j++) {
            Resultat[i][j] = Source[j][i];
        }
    }
}

int main(int argc, char* argv[]) {
    unsigned int height, width;
    unsigned int nthreads = omp_get_max_threads();

    std::string image_filename(argv[1]);
    int ITER =  atoi(argv[2]);

    get_source_params(image_filename, &height, &width);
    std::cout << width << " " << height << std::endl;
    u_char **Source, **Resultat, **ResultatGPU, **ResultatGPUShared, **ResultatGPUSharedConflictless;
    u_char *d_ResultatGPUSharedConflictless, *d_ResultatGPUShared, *d_ResultatGPU, *d_Source;

	image<u_char> imgSource(height, width, &Source);
    image<u_char> imgResultat(width, height, &Resultat);
    image<u_char> imgResultatGPU(width, height, &ResultatGPU);
    image<u_char> imgResultatGPUShared(width, height, &ResultatGPUShared);
    image<u_char> imgResultatGPUSharedConflictless(width, height, &ResultatGPUSharedConflictless);
    
       
    auto fail = init_source_image(Source, image_filename, height, width);
    if (fail) {
        std::cout << "Chargement impossible de l'image" << std::endl;
        return 0;
    }
 
    cudaMalloc(&d_Source, height*width*sizeof(u_char));    
    cudaMalloc(&d_ResultatGPU, height*width*sizeof(u_char));    
    cudaMalloc(&d_ResultatGPUShared, height*width*sizeof(u_char));    
    cudaMalloc(&d_ResultatGPUSharedConflictless, height*width*sizeof(u_char));    

    cudaMemcpy(d_Source, Source[0], height*width*sizeof(u_char), cudaMemcpyHostToDevice);

    std::chrono::high_resolution_clock::time_point t0 = std::chrono::high_resolution_clock::now();
    for (auto it =0; it < ITER; it++) { 
        cpu_transpose(Source, Resultat, height, width, nthreads);
    }
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    auto cpu_duration = std::chrono::duration<double>(t1-t0).count()/ITER;

    dim3 blocks(width/TILE_DIM, height/TILE_DIM);
    dim3 threads(TILE_DIM, BLOCK_ROWS);
    t0 = std::chrono::high_resolution_clock::now();
    for (auto it =0; it < ITER; it++) {
        gpu_transpose<<<blocks,threads>>>(d_Source, d_ResultatGPU, height, width);
        cudaDeviceSynchronize();
    }
    t1 = std::chrono::high_resolution_clock::now();
    auto gpu_duration = std::chrono::duration<double>(t1-t0).count()/ITER;
    cudaMemcpy(ResultatGPU[0], d_ResultatGPU, height*width*sizeof(u_char), cudaMemcpyDeviceToHost);


    t0 = std::chrono::high_resolution_clock::now();
    for (auto it =0; it < ITER; it++) {
        gpu_transpose_shared<<<blocks,threads>>>(d_Source, d_ResultatGPUShared, height, width);
        cudaDeviceSynchronize();
    }
    t1 = std::chrono::high_resolution_clock::now();
    auto gpu_duration_shared = std::chrono::duration<double>(t1-t0).count()/ITER;
    cudaMemcpy(ResultatGPUShared[0], d_ResultatGPUShared, height*width*sizeof(u_char), cudaMemcpyDeviceToHost);
    
    t0 = std::chrono::high_resolution_clock::now();
    for (auto it =0; it < ITER; it++) {
        gpu_transpose_shared_conflictless<<<blocks,threads>>>(d_Source, d_ResultatGPUSharedConflictless, height, width);
        cudaDeviceSynchronize();
    }
    t1 = std::chrono::high_resolution_clock::now();
    auto gpu_duration_shared_conflictless = std::chrono::duration<double>(t1-t0).count()/ITER;
    cudaMemcpy(ResultatGPUSharedConflictless[0], d_ResultatGPUSharedConflictless, height*width*sizeof(u_char), cudaMemcpyDeviceToHost);
    
    std::cout << cpu_duration / gpu_duration<< " "  << cpu_duration / gpu_duration_shared << " "  << cpu_duration / gpu_duration_shared_conflictless << std::endl;

    #ifdef DEBUG
        image_filename=std::string("images/Resultats/transpose_cpu.pgm");
        save_gray_level_image(&imgResultat, image_filename, width, height);
        image_filename=std::string("images/Resultats/transpose_gpu.pgm");
        save_gray_level_image(&imgResultatGPU, image_filename, width, height);
        image_filename=std::string("images/Resultats/transpose_gpu_shared.pgm");
        save_gray_level_image(&imgResultatGPUShared, image_filename, width, height);
        image_filename=std::string("images/Resultats/transpose_gpu_shared_conflictless.pgm");
        save_gray_level_image(&imgResultatGPUSharedConflictless, image_filename, width, height);
    #endif
    
    return 0;
}

