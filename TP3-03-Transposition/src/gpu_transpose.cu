#include "gpu_transpose.cuh"
#include "parameters.cuh"
 
__global__ void gpu_transpose(u_char *Source, u_char *Resultat, unsigned int height, unsigned int width) {
    int x = blockIdx.x * TILE_DIM + threadIdx.x;
    int y = blockIdx.y * TILE_DIM + threadIdx.y;    

    for (int j = 0; j < TILE_DIM; j+= BLOCK_ROWS)
    Resultat[x*height + (y+j)] = Source[(y+j)*width + x];
}

__global__ void gpu_transpose_shared(u_char *Source, u_char *Resultat, unsigned int height, unsigned int width) {
    __shared__ float tile[TILE_DIM][TILE_DIM];

    int x = blockIdx.x * TILE_DIM + threadIdx.x;
    int y = blockIdx.y * TILE_DIM + threadIdx.y;

    for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
        tile[threadIdx.y+j][threadIdx.x] = Source[(y+j)*width + x];

    __syncthreads();

    x = blockIdx.y * TILE_DIM + threadIdx.x;  // transpose block offset
    y = blockIdx.x * TILE_DIM + threadIdx.y;

    for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
        Resultat[(y+j)*height + x] = tile[threadIdx.x][threadIdx.y + j];
}

__global__ void gpu_transpose_shared_conflictless(u_char *Source, u_char *Resultat, unsigned int height, unsigned int width) {
    __shared__ float tile[TILE_DIM][TILE_DIM+1];

    int x = blockIdx.x * TILE_DIM + threadIdx.x;
    int y = blockIdx.y * TILE_DIM + threadIdx.y;

    for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
        tile[threadIdx.y+j][threadIdx.x] = Source[(y+j)*width + x];

    __syncthreads();

    x = blockIdx.y * TILE_DIM + threadIdx.x;  // transpose block offset
    y = blockIdx.x * TILE_DIM + threadIdx.y;

    for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
        Resultat[(y+j)*height + x] = tile[threadIdx.x][threadIdx.y + j];
}